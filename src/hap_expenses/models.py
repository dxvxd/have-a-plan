import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from hap.models.common import Nameable
from hap.models.person import PersonDate
from hap.service.utils import money_format


class Expense(Nameable):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    person_date = models.ForeignKey(PersonDate, related_name='expense')
    money = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "{} [{}]".format(self.name, str(self.money))

    def get_amount(self):
        return money_format(self.money)
