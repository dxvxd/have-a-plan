from django.contrib import admin
from .models import Action, Project, ActionItem


admin.site.register(Project)
admin.site.register(Action)
admin.site.register(ActionItem)
