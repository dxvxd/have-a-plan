import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from hap.models.common import Nameable
from hap.models.person import PersonDate


class Project(Nameable):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    code = models.CharField(max_length=20, unique=True)


class Action(Nameable):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    project = models.ForeignKey(Project)


class ActionItem(models.Model):
    STATUS_ACTIVE = 'A'
    STATUS_DONE = 'D'
    STATUS_PROGRESS = 'P'
    STATUS_TYPES = (
        (STATUS_ACTIVE, _('Active')),
        (STATUS_DONE, _('Done')),
        (STATUS_PROGRESS, _('Progress')),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    person_date = models.ForeignKey(PersonDate, related_name='item')
    action = models.ForeignKey(Action, related_name='item')
    status = models.CharField(max_length=1, choices=STATUS_TYPES, default=STATUS_ACTIVE)
    progress = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return "{}: {}".format(str(self.person_date), str(self.action))
