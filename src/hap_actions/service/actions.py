from hap_actions.models.actions import Project, Action, ActionItem
from hap.service.module import ModuleService


class ActionService(ModuleService):
    model = ActionItem

