import uuid
from django.conf import settings
from django.db import models


class Person(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True)

    def __str__(self):
        return str(self.user)


class PersonDate(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    person = models.ForeignKey(Person)
    date = models.DateField()

    class Meta:
        unique_together = ("person", "date")

    def __str__(self):
        return "{} [{}]".format(str(self.user), str(self.date))
