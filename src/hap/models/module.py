import uuid
from django.db import models
from .common import Nameable, Timeable
from .person import Person


class Module(Nameable):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    path = models.CharField(max_length=200)


class PersonModule(Timeable):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    person = models.ForeignKey(Person, related_name='modules')
    module = models.ForeignKey(Module, related_name='persons')
