import importlib
from django.utils.timezone import datetime
from hap.models.person import PersonDate
from hap.models.module import Module


class PersonSevice:
    person = None

    def __init__(self, person):
        self.person = person

    def get_lists(self, date=None):
        persondate = self.get_persondate(date)
        person_modules = self.get_modules()
        out = []
        for pmodule in person_modules:
            out.append(self.get_module_service(pmodule, persondate))
        return out

    def get_modules(self):
        return Module.objects.filter(persons__person=self.person)

    def get_module_service(self, person_module, persondate):
        module_service = getattr(importlib.import_module(person_module.path), person_module.name + "Service")
        return {'name': person_module.name, 'items': module_service(persondate).list()}

    def get_persondate(self, date=None):
        if date is None:
            date = datetime.today()
        return PersonDate.objects.get_or_create(person=self.person, date=date)
