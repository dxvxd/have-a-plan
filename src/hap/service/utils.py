

def money_format(money):
    return '{0:.2f}'.format(float(money) / 100)
