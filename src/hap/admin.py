from django.contrib import admin
from .models import Person, Module, PersonModule
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as admin_UserAdmin


class UserPersonInline(admin.StackedInline):
    model = Person


class UserAdmin(admin_UserAdmin):
    inlines = [UserPersonInline]


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Module)
admin.site.register(PersonModule)
