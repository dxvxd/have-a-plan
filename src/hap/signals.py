from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from .models import Person


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_person_handler(sender, instance, created, **kwargs):
    if not created:
        return
    # Create the profile object, only if it is newly created
    person = Person(user=instance)
    person.save()
