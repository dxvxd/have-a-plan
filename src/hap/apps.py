from django.apps import AppConfig


class PersonConfig(AppConfig):
    name = "hap"
    verbose_name = "Hap core"

    def ready(self):
        from . import signals
