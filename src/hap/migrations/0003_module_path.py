# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-03-06 13:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hap', '0002_module_personmodule'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='path',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
