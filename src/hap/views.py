from django.shortcuts import render, render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.http import HttpResponseRedirect
from .forms import LoginForm
from .service.person import PersonSevice


@login_required(login_url='/login/')
def index(request):
    person = request.user.person
    context = {
        'modules': PersonSevice(person).get_lists(),
        'module_names': [item['name'] for item in PersonSevice(person).get_lists()],
        'reminders': None,
    }
    return render(request, 'hap/index.html', context)


def login(request):
    if request.method == "POST":
        form = LoginForm(request, data=request.POST)
        if form.is_valid():
            auth_login(request, form.get_user())
            return HttpResponseRedirect(reverse('index'))
    else:
        form = LoginForm(request)

    return render(request, 'hap/login.html', {'form': form})


def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('login'))
