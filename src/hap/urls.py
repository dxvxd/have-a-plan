from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    # url(r'^authbymail/$', views.RetrieveAuthToken.as_view(), name='authbymail'),
    # url(r'^client/$', views.ClientDetails.as_view(), name='client_details'),
    # url(r'^users/$', views.PersonList.as_view(), name='users'),
    # url(r'^users/(?P<pk>[0-9a-zA-Z-]+)/devices/$', views.UserDevices.as_view(), name='user_devices'),
    # url(r'^product/types/$', views.ProductTypes.as_view(), name='product_types'),
    # url(r'^product/makers/$', views.ProductMakers.as_view(), name='product_makers'),
    # url(r'^product/lines/$', views.ProductLines.as_view(), name='product_lines'),
    # url(r'^product/models/$', views.ProductModels.as_view(), name='product_models'),
    # url(r'^device/warranty/$', views.DeviceWarranty.as_view(), name='device_warranty'),
    # url(r'^device/statuses/$', views.DeviceStatuses.as_view(), name='device_statuses'),

    # url(r'^users/', include('api.users.urls', namespace='users')),
    # url(r'^admins/', include('api.admins.urls', namespace='admins')),
    # url(r'^products/', include('api.products.urls', namespace='products')),
    # url(r'^warranty/', include('api.warranty.urls', namespace='warranty')),
    # url(r'^devices/', include('api.devices.urls', namespace='devices')),
    # url(r'^password/', include('api.password.urls', namespace='password')),
    url(r'^$', views.index, name='index'),
]
